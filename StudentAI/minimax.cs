﻿using System;
using System.Collections.Generic;
using UvsChess;
using System.Text;
using System.Diagnostics;

namespace StudentAI
{
    class minimax
    {
        const int MAX_SEARCH_DEPTH = 2;
        public static Func<bool> turnOver;

        public static ChessMove minimaxValue(DecisionTree DT, ChessBoard board, ChessColor myColor, Func<bool> isMyTurnOver)
        {
            turnOver = isMyTurnOver;
            return max(DT, board, myColor, 1, -9999, 9999);
        }

        public static ChessMove max(DecisionTree DT, ChessBoard board, ChessColor myColor, int depth,int alpha, int beta)
        {
            Random rand = new Random();
            bool depthCutoff = false;
            List<ChessMove> HighCostList = new List<ChessMove>();
            int HighestCost = -999;

            //Get the opponents color
            ChessColor oppColor;
            if (myColor == ChessColor.White)
                oppColor = ChessColor.Black;
            else
                oppColor = ChessColor.White;

            List<ChessMove> validMovesThisTurn = MoveGenerator.getAllMoves(board, myColor, false);
            List<ChessMove> legalMovesThisTurn = MoveGenerator.getAllLegalMoves(board, validMovesThisTurn, myColor);
            
            foreach (ChessMove ourMove in legalMovesThisTurn)
            {
                if (!turnOver())
                {
                    depthCutoff = false;
                    ChessBoard boardAfterMove = board.Clone();
                    boardAfterMove.MakeMove(ourMove);

                    DT.AddChild(boardAfterMove, ourMove);
                    DT = DT.LastChild;

                    //Where is their king?
                    ChessLocation king;
                    if (myColor == ChessColor.White)
                        king = Util.getKingLocation(boardAfterMove, ChessColor.Black);
                    else
                        king = Util.getKingLocation(boardAfterMove, ChessColor.White);

                    //Check for valid check flag on their king, then check if we checkmate/stalemate the opponent
                    Util.setCheckFlagForMove(board, ourMove, king, myColor);
                    Util.setCheckmateFlagForMove(boardAfterMove, ourMove, oppColor);
                    Util.setStalemateFlagForMove(boardAfterMove, ourMove, oppColor);

                    if (ourMove.Flag == ChessFlag.Checkmate)
                    {
                        ourMove.ValueOfMove += 1000;
                        //Terminal Cutoff -- Checkmate
                        return ourMove;
                    }
                    else if (ourMove.Flag == ChessFlag.Stalemate)
                    {
                        ourMove.ValueOfMove -= 1000;
                        //Depth Cutoff -- Stalemate
                        depthCutoff = true;
                    }

                    if (depth < MAX_SEARCH_DEPTH && depthCutoff == false)
                        ourMove.ValueOfMove += min(DT, boardAfterMove, oppColor, depth + 1, alpha, beta).ValueOfMove;

                    //if (depth == MAX_SEARCH_DEPTH) //Successor Function -- Heuristic Eval
                    //    ourMove.ValueOfMove = HeuristicService.getHeuristicValue(boardAfterMove, myColor, HeuristicService.HEURISTIC_TYPE.DEFAULT);

                    //Move Cost or kill
                    ChessPiece CP2 = board[ourMove.To];
                    int KillCost = 0;
                    if (CP2 != ChessPiece.Empty)
                    {
                        if (CP2 == ChessPiece.BlackPawn || CP2 == ChessPiece.WhitePawn)
                            KillCost = 10;
                        else if (CP2 == ChessPiece.BlackBishop || CP2 == ChessPiece.WhiteBishop)
                            KillCost = 40;
                        else if (CP2 == ChessPiece.BlackKnight || CP2 == ChessPiece.WhiteKnight)
                            KillCost = 40;
                        else if (CP2 == ChessPiece.BlackRook || CP2 == ChessPiece.WhiteRook)
                            KillCost = 40;
                        else if (CP2 == ChessPiece.BlackQueen || CP2 == ChessPiece.WhiteQueen)
                            KillCost = 100;
                    }
                    ourMove.ValueOfMove += KillCost;

                    //v = max(minValue(s,A,B), v)
                    if (ourMove.ValueOfMove > HighestCost)
                    {
                        HighCostList.Clear();
                        HighestCost = ourMove.ValueOfMove;
                        HighCostList.Add(ourMove);

                        //HighestCost is v.
                        //if v >= beta return v, v is ourMove value.
                        if (HighestCost > beta)
                            return ourMove;

                        //A = max(A,v)
                        if (alpha < HighestCost)
                            alpha = HighestCost;
                    }
                    else if (ourMove.ValueOfMove == HighestCost) //if equal to cost add to list of moves
                    {
                        HighCostList.Add(ourMove);
                        //HighestCost is v.
                        //if v >= beta return v, v is ourMove value.
                        if (HighestCost > beta)
                            return ourMove;

                        //A = max(A,v)
                        if (alpha < HighestCost)
                            alpha = HighestCost;
                    }
                    DT = DT.Parent;
                }
                else
                    break;
            }

            if (HighCostList.Count > 0)
            {
                DT.BestChildMove = HighCostList[rand.Next(0, HighCostList.Count)];
                return DT.BestChildMove;
            }
            else return legalMovesThisTurn[rand.Next(0, legalMovesThisTurn.Count)];
        }

        public static ChessMove min(DecisionTree DT, ChessBoard board, ChessColor myColor, int depth, int alpha, int beta)
        {
            Random rand = new Random();
            bool depthCutoff = false;
            List<ChessMove> LowCostList = new List<ChessMove>();
            int LowestCost = 999;

            //Get the opponents color
            ChessColor oppColor;
            if (myColor == ChessColor.White)
                oppColor = ChessColor.Black;
            else
                oppColor = ChessColor.White;

            List<ChessMove> validMovesThisTurn = MoveGenerator.getAllMoves(board, myColor, false);
            List<ChessMove> legalMovesThisTurn = MoveGenerator.getAllLegalMoves(board, validMovesThisTurn, myColor);

            foreach (ChessMove ourMove in legalMovesThisTurn)
            {
                if (!turnOver())
                {
                    depthCutoff = false;
                    ChessBoard boardAfterMove = board.Clone();
                    boardAfterMove.MakeMove(ourMove);

                    DT.AddChild(boardAfterMove, ourMove);
                    DT = DT.LastChild;

                    //Where is their king?
                    ChessLocation king;
                    if (myColor == ChessColor.White)
                        king = Util.getKingLocation(boardAfterMove, ChessColor.Black);
                    else
                        king = Util.getKingLocation(boardAfterMove, ChessColor.White);

                    //Check for valid check flag on their king, then check if we checkmate/stalemate the opponent
                    Util.setCheckFlagForMove(board, ourMove, king, myColor);
                    Util.setCheckmateFlagForMove(boardAfterMove, ourMove, oppColor);
                    Util.setStalemateFlagForMove(boardAfterMove, ourMove, oppColor);

                    if (ourMove.Flag == ChessFlag.Checkmate)
                    {
                        ourMove.ValueOfMove -= 1000;
                        //Terminal Cutoff -- Checkmate
                        return ourMove;
                    }
                    else if (ourMove.Flag == ChessFlag.Stalemate)
                    {
                        ourMove.ValueOfMove -= 1000;
                        //Depth Cutoff -- Stalemate
                        depthCutoff = true;
                    }

                    if (depth < MAX_SEARCH_DEPTH && depthCutoff == false)
                        ourMove.ValueOfMove += max(DT, boardAfterMove, oppColor, depth + 1, alpha, beta).ValueOfMove;

                    //if (depth == MAX_SEARCH_DEPTH) //Successor Function -- Heuristic Eval
                    //    ourMove.ValueOfMove = HeuristicService.getHeuristicValue(boardAfterMove, oppColor, HeuristicService.HEURISTIC_TYPE.DEFAULT);

                    //Move Cost or kill
                    ChessPiece CP2 = board[ourMove.To];
                    int KillCost = 0;
                    if (CP2 != ChessPiece.Empty)
                    {
                        if (CP2 == ChessPiece.BlackPawn || CP2 == ChessPiece.WhitePawn)
                            KillCost = 10;
                        else if (CP2 == ChessPiece.BlackBishop || CP2 == ChessPiece.WhiteBishop)
                            KillCost = 40;
                        else if (CP2 == ChessPiece.BlackKnight || CP2 == ChessPiece.WhiteKnight)
                            KillCost = 40;
                        else if (CP2 == ChessPiece.BlackRook || CP2 == ChessPiece.WhiteRook)
                            KillCost = 40;
                        else if (CP2 == ChessPiece.BlackQueen || CP2 == ChessPiece.WhiteQueen)
                            KillCost = 100;
                    }
                    ourMove.ValueOfMove -= KillCost;

                    //v= min(maxValue(s,A,B), v)
                    if (ourMove.ValueOfMove < LowestCost)
                    {
                        LowCostList.Clear();
                        LowestCost = ourMove.ValueOfMove;
                        LowCostList.Add(ourMove);

                        //if v <= alpha return v, v is ourMove value.
                        if (LowestCost < alpha)
                            return ourMove;

                        //B = min(B,v)
                        if (beta > LowestCost)
                            beta = LowestCost;
                    }
                    else if (ourMove.ValueOfMove == LowestCost) //if equal to cost add to list of moves
                    {
                        LowCostList.Add(ourMove);
                        //LowestCost is v.
                        //if v <= alpha return v, v is ourMove value.
                        if (LowestCost < alpha)
                            return ourMove;

                        //B = min(B,v)
                        if (beta > LowestCost)
                            beta = LowestCost;
                    }
                    DT = DT.Parent;
                }
                else
                    break;
            }

            if (LowCostList.Count > 0)
            {
                DT.BestChildMove = LowCostList[rand.Next(0, LowCostList.Count)];
                return DT.BestChildMove;
            }
            else return legalMovesThisTurn[rand.Next(0, legalMovesThisTurn.Count)];
        }
    }
}
