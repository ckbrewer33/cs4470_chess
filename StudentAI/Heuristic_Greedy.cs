﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UvsChess;

namespace StudentAI
{
    class Heuristic_Greedy
    {
        private Heuristic_Greedy() { }

        public static int GetHeuristicValue(ChessBoard board, ChessColor myColor)
        {
            List<ChessMove> Moves = MoveGenerator.getAllLegalMoves(board, MoveGenerator.getAllMoves(board, myColor, false), myColor);

            int i = 0;
            int HighestCost = 0;
            List<int> HighCostList = new List<int>();

            //Get the opponents color
            ChessColor oppColor;
            if (myColor == ChessColor.White)
                oppColor = ChessColor.Black;
            else
                oppColor = ChessColor.White;

            List<ChessMove> validMovesThisTurn = MoveGenerator.getAllMoves(board, oppColor, false);
            List<ChessMove> legalMovesThisTurn = MoveGenerator.getAllLegalMoves(board, validMovesThisTurn, oppColor);

            foreach (ChessMove Move in Moves)
            {
                int cost = 0;

                //Piece Cost
                ChessPiece CP = board[Move.From];
                if (CP == ChessPiece.WhitePawn)
                {
                    cost += ((int)CP % 6);// + 1;
                }
                else if (CP == ChessPiece.BlackPawn)
                {
                    cost += ((int)CP % 6) + 1;// +2;
                }


                //Move Cost or kill
                ChessPiece CP2 = board[Move.To];
                int KillCost = 0;
                if (CP2 != ChessPiece.Empty)
                {
                    if (Util.PIECE_COLOR_MAP[CP2] == ChessColor.Black)
                        KillCost = ((int)CP2 + 1) + 5;
                    else if (Util.PIECE_COLOR_MAP[CP2] == ChessColor.White)
                        KillCost = ((int)CP2 % 6) + 5;
                }
                else // blank space
                {
                    KillCost = (int)CP2 % 6;
                }
                cost += KillCost;

                //See if our piece is currently threatened.
                int oppCostThisTurn = 0;
                foreach (ChessMove oppMove in legalMovesThisTurn)
                {
                    //Move Protection
                    if (oppMove.To == Move.From)
                    {
                        ChessPiece CPOppThisTurn = board[Move.To];

                        //Will our move allow an opponent to kill our piece
                        if (CPOppThisTurn != ChessPiece.Empty)
                        {
                            if (Util.PIECE_COLOR_MAP[CPOppThisTurn] == ChessColor.Black)
                            {
                                oppCostThisTurn = ((int)CPOppThisTurn + 1) + 5;
                            }
                            else if (Util.PIECE_COLOR_MAP[CPOppThisTurn] == ChessColor.White)
                            {
                                oppCostThisTurn = ((int)CPOppThisTurn % 6) + 5;
                            }
                        }
                        else // blank space
                            oppCostThisTurn = (int)CPOppThisTurn % 6;
                    }
                }

                //A piece is threatened, let's move it if we can.
                cost += oppCostThisTurn;

                ChessBoard boardAfterMove = board.Clone();
                boardAfterMove.MakeMove(Move);

                //Where is our king?
                ChessLocation king;
                if (myColor == ChessColor.White)
                    king = Util.getKingLocation(boardAfterMove, ChessColor.Black);
                else
                    king = Util.getKingLocation(boardAfterMove, ChessColor.White);

                if (king == null)
                    king = Util.getKingLocation(boardAfterMove, myColor);
                if (king == Move.To)
                    Move.Flag = ChessFlag.Checkmate;

                //Check for valid check flag on our king, then check if we checkmate/stalemate the opponent
                Util.setCheckFlagForMove(board, Move, king, myColor);
                Util.setCheckmateFlagForMove(boardAfterMove, Move, oppColor);
                Util.setStalemateFlagForMove(boardAfterMove, Move, oppColor);

                //if we can checkmate them do it
                if (Move.Flag == ChessFlag.Checkmate)
                {
                    cost += 100;
                }
                //Try to avoid stalemates
                else if (Move.Flag == ChessFlag.Stalemate)
                {
                    cost -= 100;
                }
                else //if not check their moves
                {
                    if (Move.Flag == ChessFlag.Check)
                    {
                        cost += 5;
                    }

                    //Checking Opponents possible moves
                    List<ChessMove> validMoves = MoveGenerator.getAllMoves(boardAfterMove, oppColor, false);
                    List<ChessMove> legalMoves = MoveGenerator.getAllLegalMoves(boardAfterMove, validMoves, oppColor);
                    int oppCost = 0;
                    foreach (ChessMove oppMove in legalMoves)
                    {
                        //Move Protection -- Our piece is threatened on their next turn.
                        if (oppMove.To == Move.To)
                        {
                            ChessPiece CPOpp = boardAfterMove[Move.To];

                            //Will our move allow an opponent to kill our piece we decided to move
                            if (CPOpp != ChessPiece.Empty)
                            {
                                if (Util.PIECE_COLOR_MAP[CPOpp] == ChessColor.Black)
                                {
                                    oppCost = ((int)CPOpp + 1) + 5;
                                }
                                else if (Util.PIECE_COLOR_MAP[CPOpp] == ChessColor.White)
                                {
                                    oppCost = ((int)CPOpp % 6) + 5;
                                }
                            }
                            else // blank space
                                oppCost = (int)CPOpp % 6;
                        }

                    }
                    //If so lets reduce the cost for that move (might still be a good move)
                    cost -= oppCost;
                }

                //if Highest replace the list of moves
                if (cost > HighestCost)
                {
                    HighCostList.Clear();
                    HighestCost = cost;
                    HighCostList.Add(i);
                }
                else if (cost == HighestCost) //if equal to cost add to list of moves
                {
                    HighCostList.Add(i);
                }

                i++;
            }

            //We apparently don't have any good moves, the piece we move will end up being killed. (most like our king)
            if (HighCostList.Count == 0)
            {
                return 0;
            }

            //Return a random move of equal cost that was considered our highest cost moves.
            Random rand = new Random();
            return HighestCost;// HighCostList[rand.Next(0, HighCostList.Count)];
        }
    }
}
