﻿using UvsChess;
using System.Collections.Generic;
using System;
using System.Linq;


namespace StudentAI
{
    class Chris_minimax
    {
        const int MAX_SEARCH_DEPTH = 3;
        static Func<bool> IsMyTurnOver;

        public static ChessMove getNextMove(ChessBoard board, ChessColor myColor, HeuristicService.HEURISTIC_TYPE heuristicType, Func<bool> isMyTurnOver)
        {
            IsMyTurnOver = isMyTurnOver;
            List<ChessMove> originalMoves = MoveGenerator.getAllLegalMoves(board, MoveGenerator.getAllMoves(board, myColor, false), myColor);
            ChessColor opponentColor = myColor == ChessColor.Black ? ChessColor.White : ChessColor.Black;
            ChessPiece opponentKingPiece = myColor == ChessColor.Black ? ChessPiece.WhiteKing : ChessPiece.BlackKing;

            int[] originalMoveValues = new int[originalMoves.Count];
            List<ChessMove> bestMoves = new List<ChessMove>();
            ChessMove bestMove = null;
            int bestMoveValue = -1000;

            // Run minimax to get the cost of all the available moves
            //for (int iteratorDeep = 1; iteratorDeep <= MAX_SEARCH_DEPTH; iteratorDeep++)
            //{
                // For each move in the original moves, make that move and the costs of the resulting board through minimax
                for (int i = 0; i < originalMoves.Count; i++)
                {
                    ChessBoard boardAfterMove = board.Clone();
                    boardAfterMove.MakeMove(originalMoves[i]);

                    int moveValue = minimax_Chris(boardAfterMove, MAX_SEARCH_DEPTH, -1000, 1000, false, myColor, heuristicType);
                    if (moveValue != 1000)
                    {
                        originalMoveValues[i] = moveValue;
                    }
                }
            //}

            // Find the max value calculated by minimax
            for (int j = 0; j < originalMoves.Count; j++)
            {
                if (originalMoveValues[j] > bestMoveValue)
                {
                    bestMoveValue = originalMoveValues[j];
                }
            }

            // Get all the moves that have the value of the best move value calculated
            for (int k = 0; k < originalMoves.Count; k++)
            {
                if (originalMoveValues[k] == bestMoveValue)
                {
                    bestMoves.Add(originalMoves[k]);
                }
            }

            Random rand = new Random();
            bestMove = bestMoves.ElementAt(rand.Next(0, bestMoves.Count));
            ChessBoard boardAfterBestMove = board.Clone();
            boardAfterBestMove.MakeMove(bestMove);
            ChessLocation opponentKingLocation = Util.getKingLocation(boardAfterBestMove, opponentColor);

            if (Util.isInCheck(boardAfterBestMove, opponentKingLocation))
            {
                bestMove.Flag = ChessFlag.Check;
            }

            //Util.setCheckFlagForMove(board, bestMove, opponentKingLocation, myColor);
            Util.setCheckmateFlagForMove(board, bestMove, opponentColor);
            Util.setStalemateFlagForMove(boardAfterBestMove, bestMove, opponentColor);

            return bestMove;
        }

        public static int minimax_Chris(ChessBoard board, int depthRemaining, int alpha, int beta, bool isMaxTurn, ChessColor colorOfCurrentMover, HeuristicService.HEURISTIC_TYPE heuristicType)
        {
            if (depthRemaining == 0)
            {
                return HeuristicService.getHeuristicValue(board, colorOfCurrentMover, heuristicType);
            }

            // Run Max's turn
            if (isMaxTurn)
            {
                List<ChessMove> possibleMoves = MoveGenerator.getAllLegalMoves(board, MoveGenerator.getAllMoves(board, colorOfCurrentMover, false), colorOfCurrentMover);

                int maxValue = -10000, v = 0;

                foreach (ChessMove move in possibleMoves)
                {
                    //StudentAI.DT.AddChild(board, move);
                    if (!IsMyTurnOver()) // Change this to !IsMyTurnOver() when being used for real so we don't go over time
                    {
                        ChessBoard clonedBoard = board.Clone();
                        clonedBoard.MakeMove(move);

                        //StudentAI.DT = StudentAI.DT.LastChild;

                        v = Math.Max(alpha, minimax_Chris(clonedBoard, depthRemaining - 1, alpha, beta, false, colorOfCurrentMover, heuristicType));
                        v += HeuristicService.getHeuristicValue(board, colorOfCurrentMover, heuristicType);

                        //StudentAI.DT.EventualMoveValue = v.ToString();

                        if (v >= beta)
                        {
                            //StudentAI.DT.BestChildMove = move;
                            return v;
                        }

                        if (maxValue < v)
                        {
                            maxValue = v;
                        }
                    }
                    //return maxValue;
                }
                return maxValue;
                // Modify the cost for checks
                //if (possibleMoves.Count == 0 && Util.isInCheck(board, Util.getKingLocation(board, colorOfCurrentMover == ChessColor.White ? ChessColor.White : ChessColor.Black)))
                //{
                //    alpha = -13 + HeuristService.getHeuristicValue(board, colorOfCurrentMover, heuristicType);
                //}

                //return alpha;
            }

            // Run Min's turn
            else
            {
                List<ChessMove> possibleMoves = MoveGenerator.getAllLegalMoves(board, MoveGenerator.getAllMoves(board, colorOfCurrentMover, false), colorOfCurrentMover);
                int minValue = 10000, v = 0;
                foreach (ChessMove move in possibleMoves)
                {
                    //StudentAI.DT.AddChild(board, move);
                    if (!IsMyTurnOver()) // Change this to !IsMyTurnOver() when being used for real so we don't go over time
                    {
                        ChessBoard clonedBoard = board.Clone();
                        clonedBoard.MakeMove(move);

                        //StudentAI.DT = StudentAI.DT.LastChild;

                        v = Math.Min(beta, minimax_Chris(clonedBoard, depthRemaining - 1, alpha, beta, true, colorOfCurrentMover, heuristicType));

                        //StudentAI.DT.EventualMoveValue = v.ToString();
                        
                        if (v <= alpha)
                        {
                            //StudentAI.DT.BestChildMove = move;
                            return v;
                        }

                        if (minValue > v)
                        {
                            minValue = v;
                        }
                    }
                }

                return minValue;

                // Modify the cost for checks
                //if (possibleMoves.Count == 0 && Util.isInCheck(board, Util.getKingLocation(board, colorOfCurrentMover == ChessColor.White ? ChessColor.Black : ChessColor.White)))
                //{
                //    beta = 13 + HeuristService.getHeuristicValue(board, colorOfCurrentMover, heuristicType);
                //}

                //return beta;
            }
            
        }

    } // end class
} // end namespace
