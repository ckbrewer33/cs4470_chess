﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UvsChess;
using StudentAI;
using System.Collections.Generic;

namespace ChessAITests
{
    [TestClass]
    public class MoveGeneratorTests
    {
        private ChessBoard testboard;

        private void clearTestBoard()
        {
            for (int row = 0; row < ChessBoard.NumberOfRows; row++)
            {
                for (int column = 0; column < ChessBoard.NumberOfColumns; column++)    
                {
                    testboard[row, column] = ChessPiece.Empty;
                }
            }
        }

        [TestMethod]
        public void testGetWhitePawnMovesReturnsValidMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testPawnLocation = new ChessLocation(1, 6);

            // Test forward moves when not blocked
            clearTestBoard();
            testboard[testPawnLocation] = ChessPiece.WhitePawn;
            ChessMove expectedMove1 = new ChessMove(testPawnLocation, new ChessLocation(1, 4)); // Two spaces forward
            ChessMove expectedMove2 = new ChessMove(testPawnLocation, new ChessLocation(1, 5)); // One space forward
            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.WhitePawn, testPawnLocation, false);
            Assert.AreEqual(2, validMoves.Count, "There should be two available moves for this test");
            Assert.IsTrue(validMoves.Contains(expectedMove1), "White pawn should be able to move 2 spaces on first turn");
            Assert.IsTrue(validMoves.Contains(expectedMove2), "White pawn should be able to move 1 space forward when not blocked");

            // Test no forward moves when white pawn is blocked
            clearTestBoard();
            testboard[testPawnLocation] = ChessPiece.WhitePawn;
            testboard[1, 5] = ChessPiece.BlackPawn;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.WhitePawn, testPawnLocation, false);
            Assert.AreEqual(0, validMoves.Count, "There should be no available moves for this test");
            Assert.IsFalse(validMoves.Contains(expectedMove1), "White pawn should be not able to move 2 spaces on first turn when blocked");
            Assert.IsFalse(validMoves.Contains(expectedMove2), "White pawn should be not able to move 1 space forward when blocked");

            // Test can capture diagonally
            clearTestBoard();
            testboard[testPawnLocation] = ChessPiece.WhitePawn;
            testboard[0, 5] = ChessPiece.BlackPawn;
            testboard[2, 5] = ChessPiece.BlackPawn;
            ChessMove captureMove1 = new ChessMove(new ChessLocation(1, 6), new ChessLocation(0, 5)); // Capture forward diagonal left
            ChessMove captureMove2 = new ChessMove(new ChessLocation(1, 6), new ChessLocation(2, 5)); // Capture forward diagonal right
            expectedMove1 = new ChessMove(new ChessLocation(1, 6), new ChessLocation(1, 4)); // Two spaces forward
            expectedMove2 = new ChessMove(new ChessLocation(1, 6), new ChessLocation(1, 5)); // One space forward
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.WhitePawn, testPawnLocation, false);

            Assert.AreEqual(4, validMoves.Count, "There should be 4 available moves for this test");
            Assert.IsTrue(validMoves.Contains(captureMove1), "White pawn should be able to capture to forward diagonal left");
            Assert.IsTrue(validMoves.Contains(captureMove2), "White pawn should be able to capture to forward diagonal right");
            Assert.IsTrue(validMoves.Contains(expectedMove1), "White pawn should be able to move 2 spaces on first turn");
            Assert.IsTrue(validMoves.Contains(expectedMove2), "White pawn should be able to move 1 space forward when not blocked");

            // Test should not try capturing teammate
            clearTestBoard();
            testboard[1, 6] = ChessPiece.WhitePawn;
            testboard[0, 5] = ChessPiece.WhitePawn;
            testboard[1, 5] = ChessPiece.WhitePawn;
            testboard[2, 5] = ChessPiece.WhitePawn;

            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.WhitePawn, testPawnLocation, false);
            Assert.AreEqual(0, validMoves.Count, "There should be no available moves for this test");
            Assert.IsFalse(validMoves.Contains(captureMove1), "White pawn should be not capture teammate");
            Assert.IsFalse(validMoves.Contains(captureMove2), "White pawn should be not capture teammate");
        }

        [TestMethod]
        public void testGetWhitePawnMovesReturnsProtectionMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testPawnLocation = new ChessLocation(1, 6);

            // Test diagonal protection move
            clearTestBoard();
            testboard[testPawnLocation] = ChessPiece.WhitePawn;
            testboard[0, 5] = ChessPiece.WhitePawn;
            testboard[2, 5] = ChessPiece.WhitePawn;
            ChessMove protectionMove1 = new ChessMove(new ChessLocation(1, 6), new ChessLocation(0, 5)); // Capture forward diagonal left
            ChessMove protectionMove2 = new ChessMove(new ChessLocation(1, 6), new ChessLocation(2, 5)); // Capture forward diagonal right

            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.WhitePawn, testPawnLocation, true);

            Assert.AreEqual(4, validMoves.Count, "There should be 4 available moves for this test");
            Assert.IsTrue(validMoves.Contains(protectionMove1), "White pawn should be able to capture to forward diagonal left");
            Assert.IsTrue(validMoves.Contains(protectionMove2), "White pawn should be able to capture to forward diagonal right");

        }

        [TestMethod]
        public void testGetBlackPawnMovesReturnsValidMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testPawnPosition = new ChessLocation(1,1);

            // Test forward movements
            clearTestBoard();
            testboard[testPawnPosition] = ChessPiece.BlackPawn;
            ChessMove expectedMove1 = new ChessMove(testPawnPosition, new ChessLocation(1, 2));
            ChessMove expectedMove2 = new ChessMove(testPawnPosition, new ChessLocation(1, 3));
            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackPawn, testPawnPosition, false);
            Assert.AreEqual(2, validMoves.Count, "There should be 2 valid pawn moves for this test");
            Assert.IsTrue(validMoves.Contains(expectedMove1), "Black pawn should be able to move forwared one space when not blocked");
            Assert.IsTrue(validMoves.Contains(expectedMove2), "Black pawn should be able to move forwared two spaces from start position when not blocked");

            // Test forward movements when blocked
            clearTestBoard();
            testboard[testPawnPosition] = ChessPiece.BlackPawn;
            testboard[1, 3] = ChessPiece.WhitePawn;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackPawn, testPawnPosition, false);
            Assert.AreEqual(1, validMoves.Count, "There should be 1 valid pawn moves for this test");
            Assert.IsFalse(validMoves.Contains(expectedMove2), "Black pawn should not move forwared two spaces from start position when blocked");
            Assert.IsTrue(validMoves.Contains(expectedMove1), "Black pawn should be able to move forwared one space when not blocked");

            clearTestBoard();
            testboard[testPawnPosition] = ChessPiece.BlackPawn;
            testboard[1, 2] = ChessPiece.WhitePawn;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackPawn, testPawnPosition, false);
            Assert.AreEqual(0, validMoves.Count, "There should be 0 valid pawn moves for this test");
            Assert.IsFalse(validMoves.Contains(expectedMove2), "Black pawn should not move forwared two spaces from start position when blocked");
            Assert.IsFalse(validMoves.Contains(expectedMove1), "Black pawn should not be able to move forwared one space when blocked");

            // Test capture moves
            clearTestBoard();
            testboard[testPawnPosition] = ChessPiece.BlackPawn;
            testboard[0, 2] = ChessPiece.WhitePawn;
            testboard[1, 2] = ChessPiece.WhitePawn;
            testboard[2, 2] = ChessPiece.WhitePawn;
            ChessMove capturemove1 = new ChessMove(testPawnPosition, new ChessLocation(0, 2));
            ChessMove capturemove2 = new ChessMove(testPawnPosition, new ChessLocation(2, 2));
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackPawn, testPawnPosition, false);
            Assert.AreEqual(2, validMoves.Count, "There should be 2 valid pawn moves for this test");
            Assert.IsTrue(validMoves.Contains(capturemove1), "Black pawn should be able to capture on a forward left move");
            Assert.IsTrue(validMoves.Contains(capturemove2), "Black pawn should be able to capture on a forward right move");

            // Test doesn't caputre friendly pieces
            clearTestBoard();
            testboard[testPawnPosition] = ChessPiece.BlackPawn;
            testboard[0, 2] = ChessPiece.BlackPawn;
            testboard[1, 2] = ChessPiece.BlackPawn;
            testboard[2, 2] = ChessPiece.BlackPawn;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackPawn, testPawnPosition, false);
            Assert.AreEqual(0, validMoves.Count, "There should be 0 valid pawn moves for this test");
            Assert.IsFalse(validMoves.Contains(capturemove1), "Black pawn should not capture a friendly piece");
            Assert.IsFalse(validMoves.Contains(capturemove2), "Black pawn should not capture a friendly piece");
        }

        [TestMethod]
        public void testGetBlackPawnMovesReturnsProtectionMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testPawnPosition = new ChessLocation(1, 1);

            // Test doesn't caputre friendly pieces
            clearTestBoard();
            testboard[testPawnPosition] = ChessPiece.BlackPawn;
            testboard[0, 2] = ChessPiece.BlackPawn;
            testboard[1, 2] = ChessPiece.BlackPawn;
            testboard[2, 2] = ChessPiece.BlackPawn;

            ChessMove protectionmove1 = new ChessMove(testPawnPosition, new ChessLocation(0, 2));
            ChessMove protectionmove2 = new ChessMove(testPawnPosition, new ChessLocation(2, 2));
            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackPawn, testPawnPosition, true);

            Assert.AreEqual(2, validMoves.Count, "There should be 2 valid pawn moves for this test");
            Assert.IsTrue(validMoves.Contains(protectionmove1), "Black pawn should protect a friendly piece");
            Assert.IsTrue(validMoves.Contains(protectionmove2), "Black pawn should protect a friendly piece");
        }

        

        [TestMethod]
        public void testGetRookMovesReturnsValidMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testRookLocation = new ChessLocation(4, 4);

            // Test all linear moves
            clearTestBoard();
            testboard[testRookLocation] = ChessPiece.BlackRook;
            ChessMove possiblemove1 = new ChessMove(testRookLocation, new ChessLocation(4, 0));
            ChessMove possiblemove2 = new ChessMove(testRookLocation, new ChessLocation(4, 1));
            ChessMove possiblemove3 = new ChessMove(testRookLocation, new ChessLocation(4, 2));
            ChessMove possiblemove4 = new ChessMove(testRookLocation, new ChessLocation(4, 3));
            ChessMove possiblemove5 = new ChessMove(testRookLocation, new ChessLocation(4, 5));
            ChessMove possiblemove6 = new ChessMove(testRookLocation, new ChessLocation(4, 6));
            ChessMove possiblemove7 = new ChessMove(testRookLocation, new ChessLocation(4, 7));
            ChessMove possiblemove8 = new ChessMove(testRookLocation, new ChessLocation(0, 4));
            ChessMove possiblemove9 = new ChessMove(testRookLocation, new ChessLocation(1, 4));
            ChessMove possiblemove10 = new ChessMove(testRookLocation, new ChessLocation(2, 4));
            ChessMove possiblemove11 = new ChessMove(testRookLocation, new ChessLocation(3, 4));
            ChessMove possiblemove12 = new ChessMove(testRookLocation, new ChessLocation(5, 4));
            ChessMove possiblemove13 = new ChessMove(testRookLocation, new ChessLocation(6, 4));
            ChessMove possiblemove14 = new ChessMove(testRookLocation, new ChessLocation(7, 4));
            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackRook, testRookLocation, false);

            Assert.AreEqual(14, validMoves.Count, "Should have 14 possible moves for this rook test");
            Assert.IsTrue(validMoves.Contains(possiblemove1));
            Assert.IsTrue(validMoves.Contains(possiblemove2));
            Assert.IsTrue(validMoves.Contains(possiblemove3));
            Assert.IsTrue(validMoves.Contains(possiblemove4));
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove6));
            Assert.IsTrue(validMoves.Contains(possiblemove7));
            Assert.IsTrue(validMoves.Contains(possiblemove8));
            Assert.IsTrue(validMoves.Contains(possiblemove9));
            Assert.IsTrue(validMoves.Contains(possiblemove10));
            Assert.IsTrue(validMoves.Contains(possiblemove11));
            Assert.IsTrue(validMoves.Contains(possiblemove12));
            Assert.IsTrue(validMoves.Contains(possiblemove13));
            Assert.IsTrue(validMoves.Contains(possiblemove14));

            // Test blocked linear moves
            clearTestBoard();
            testboard[testRookLocation] = ChessPiece.BlackRook;
            testboard[2, 4] = ChessPiece.BlackRook;
            testboard[4, 3] = ChessPiece.BlackRook;
            testboard[7, 4] = ChessPiece.BlackRook;
            testboard[4, 7] = ChessPiece.BlackRook;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackRook, testRookLocation, false);

            Assert.AreEqual(5, validMoves.Count, "Should have 5 possible moves for this rook test");
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove6));
            Assert.IsTrue(validMoves.Contains(possiblemove11));
            Assert.IsTrue(validMoves.Contains(possiblemove12));
            Assert.IsTrue(validMoves.Contains(possiblemove13));

            // Test capture linear moves
            clearTestBoard();
            testboard[testRookLocation] = ChessPiece.BlackRook;
            testboard[2, 4] = ChessPiece.WhiteRook;
            testboard[4, 3] = ChessPiece.WhiteRook;
            testboard[7, 4] = ChessPiece.WhiteRook;
            testboard[4, 7] = ChessPiece.WhiteRook;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackRook, testRookLocation, false);

            Assert.AreEqual(9, validMoves.Count, "Should have 9 possible moves for this rook test");
            Assert.IsTrue(validMoves.Contains(possiblemove4));
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove6));
            Assert.IsTrue(validMoves.Contains(possiblemove7));
            Assert.IsTrue(validMoves.Contains(possiblemove10));
            Assert.IsTrue(validMoves.Contains(possiblemove11));
            Assert.IsTrue(validMoves.Contains(possiblemove12));
            Assert.IsTrue(validMoves.Contains(possiblemove13));
            Assert.IsTrue(validMoves.Contains(possiblemove14));

        }

        [TestMethod]
        public void testRookReturnsProtectionMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testRookLocation = new ChessLocation(4, 4);

            // Test protection moves are returned when requested
            clearTestBoard();
            testboard[testRookLocation] = ChessPiece.BlackRook;
            testboard[4, 3] = ChessPiece.BlackRook;
            testboard[3, 4] = ChessPiece.BlackRook;
            testboard[5, 4] = ChessPiece.BlackRook;
            testboard[4, 5] = ChessPiece.BlackRook;

            ChessMove protectionMove1 = new ChessMove(testRookLocation, new ChessLocation(4, 3));
            ChessMove protectionMove2 = new ChessMove(testRookLocation, new ChessLocation(3, 4));
            ChessMove protectionMove3 = new ChessMove(testRookLocation, new ChessLocation(5, 4));
            ChessMove protectionMove4 = new ChessMove(testRookLocation, new ChessLocation(4, 5));

            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackRook, testRookLocation, true);

            Assert.AreEqual(4, validMoves.Count, "Should have 4 possible moves for this rook test");
            Assert.IsTrue(validMoves.Contains(protectionMove1));
            Assert.IsTrue(validMoves.Contains(protectionMove2));
            Assert.IsTrue(validMoves.Contains(protectionMove3));
            Assert.IsTrue(validMoves.Contains(protectionMove4));

        }

        [TestMethod]
        public void testGetKnightMovesReturnsValidMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testKnightLocation = new ChessLocation(4,4);

            // Test movements to all possible positions
            clearTestBoard();
            testboard[testKnightLocation] = ChessPiece.BlackKnight;
            ChessMove expectedMove1 = new ChessMove(testKnightLocation, new ChessLocation(3, 2));
            ChessMove expectedMove2 = new ChessMove(testKnightLocation, new ChessLocation(2, 3));
            ChessMove expectedMove3 = new ChessMove(testKnightLocation, new ChessLocation(3, 6));
            ChessMove expectedMove4 = new ChessMove(testKnightLocation, new ChessLocation(2, 5));
            ChessMove expectedMove5 = new ChessMove(testKnightLocation, new ChessLocation(5, 2));
            ChessMove expectedMove6 = new ChessMove(testKnightLocation, new ChessLocation(6, 3));
            ChessMove expectedMove7 = new ChessMove(testKnightLocation, new ChessLocation(5, 6));
            ChessMove expectedMove8 = new ChessMove(testKnightLocation, new ChessLocation(6, 5));
            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackKnight, testKnightLocation, false);
            Assert.AreEqual(8, validMoves.Count, "There should be 8 moves for this knight test");
            Assert.IsTrue(validMoves.Contains(expectedMove1));
            Assert.IsTrue(validMoves.Contains(expectedMove2));
            Assert.IsTrue(validMoves.Contains(expectedMove3));
            Assert.IsTrue(validMoves.Contains(expectedMove4));
            Assert.IsTrue(validMoves.Contains(expectedMove5));
            Assert.IsTrue(validMoves.Contains(expectedMove6));
            Assert.IsTrue(validMoves.Contains(expectedMove7));
            Assert.IsTrue(validMoves.Contains(expectedMove8));

            // Test movements when pieces can be captured
            clearTestBoard();
            testboard[testKnightLocation] = ChessPiece.BlackKnight;
            testboard[3, 2] = ChessPiece.WhiteKnight;
            testboard[2, 3] = ChessPiece.WhiteKnight;
            testboard[3, 6] = ChessPiece.WhiteKnight;
            testboard[2, 5] = ChessPiece.WhiteKnight;
            testboard[5, 2] = ChessPiece.WhiteKnight;
            testboard[6, 3] = ChessPiece.WhiteKnight;
            testboard[5, 6] = ChessPiece.WhiteKnight;
            testboard[6, 5] = ChessPiece.WhiteKnight;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackKnight, testKnightLocation, false);
            Assert.AreEqual(8, validMoves.Count, "There should be 8 moves for this knight test");
            Assert.IsTrue(validMoves.Contains(expectedMove1));
            Assert.IsTrue(validMoves.Contains(expectedMove2));
            Assert.IsTrue(validMoves.Contains(expectedMove3));
            Assert.IsTrue(validMoves.Contains(expectedMove4));
            Assert.IsTrue(validMoves.Contains(expectedMove5));
            Assert.IsTrue(validMoves.Contains(expectedMove6));
            Assert.IsTrue(validMoves.Contains(expectedMove7));
            Assert.IsTrue(validMoves.Contains(expectedMove8));

            // Test movements when destinations are blocked
            clearTestBoard();
            testboard[testKnightLocation] = ChessPiece.BlackKnight;
            testboard[3, 2] = ChessPiece.BlackKnight;
            testboard[2, 3] = ChessPiece.BlackKnight;
            testboard[3, 6] = ChessPiece.BlackKnight;
            testboard[2, 5] = ChessPiece.BlackKnight;
            testboard[5, 2] = ChessPiece.BlackKnight;
            testboard[6, 3] = ChessPiece.BlackKnight;
            testboard[5, 6] = ChessPiece.BlackKnight;
            testboard[6, 5] = ChessPiece.BlackKnight;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackKnight, testKnightLocation, false);
            Assert.AreEqual(0, validMoves.Count, "There should be 0 moves for this knight test");
            Assert.IsFalse(validMoves.Contains(expectedMove1));
            Assert.IsFalse(validMoves.Contains(expectedMove2));
            Assert.IsFalse(validMoves.Contains(expectedMove3));
            Assert.IsFalse(validMoves.Contains(expectedMove4));
            Assert.IsFalse(validMoves.Contains(expectedMove5));
            Assert.IsFalse(validMoves.Contains(expectedMove6));
            Assert.IsFalse(validMoves.Contains(expectedMove7));
            Assert.IsFalse(validMoves.Contains(expectedMove8));
            
        }

        [TestMethod]
        public void testKnightReturnsProtectionMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testKnightLocation = new ChessLocation(4, 4);

            // Test movements when pieces can be captured
            clearTestBoard();
            testboard[testKnightLocation] = ChessPiece.BlackKnight;
            testboard[3, 2] = ChessPiece.BlackKnight;
            testboard[2, 3] = ChessPiece.BlackKnight;
            testboard[3, 6] = ChessPiece.BlackKnight;
            testboard[2, 5] = ChessPiece.BlackKnight;
            testboard[5, 2] = ChessPiece.BlackKnight;
            testboard[6, 3] = ChessPiece.BlackKnight;
            testboard[5, 6] = ChessPiece.BlackKnight;
            testboard[6, 5] = ChessPiece.BlackKnight;
            ChessMove expectedMove1 = new ChessMove(testKnightLocation, new ChessLocation(3, 2));
            ChessMove expectedMove2 = new ChessMove(testKnightLocation, new ChessLocation(2, 3));
            ChessMove expectedMove3 = new ChessMove(testKnightLocation, new ChessLocation(3, 6));
            ChessMove expectedMove4 = new ChessMove(testKnightLocation, new ChessLocation(2, 5));
            ChessMove expectedMove5 = new ChessMove(testKnightLocation, new ChessLocation(5, 2));
            ChessMove expectedMove6 = new ChessMove(testKnightLocation, new ChessLocation(6, 3));
            ChessMove expectedMove7 = new ChessMove(testKnightLocation, new ChessLocation(5, 6));
            ChessMove expectedMove8 = new ChessMove(testKnightLocation, new ChessLocation(6, 5));

            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackKnight, testKnightLocation, true);

            Assert.AreEqual(8, validMoves.Count, "There should be 8 moves for this knight test");
            Assert.IsTrue(validMoves.Contains(expectedMove1));
            Assert.IsTrue(validMoves.Contains(expectedMove2));
            Assert.IsTrue(validMoves.Contains(expectedMove3));
            Assert.IsTrue(validMoves.Contains(expectedMove4));
            Assert.IsTrue(validMoves.Contains(expectedMove5));
            Assert.IsTrue(validMoves.Contains(expectedMove6));
            Assert.IsTrue(validMoves.Contains(expectedMove7));
            Assert.IsTrue(validMoves.Contains(expectedMove8));
        }

        [TestMethod]
        public void testGetBishopMovesReturnsValidMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testBishopLocation = new ChessLocation(4, 4);

            // Test diagonal movement
            clearTestBoard();
            testboard[testBishopLocation] = ChessPiece.BlackBishop;
            ChessMove possiblemove1 = new ChessMove(testBishopLocation, new ChessLocation(0, 0));
            ChessMove possiblemove2 = new ChessMove(testBishopLocation, new ChessLocation(1, 1));
            ChessMove possiblemove3 = new ChessMove(testBishopLocation, new ChessLocation(2, 2));
            ChessMove possiblemove4 = new ChessMove(testBishopLocation, new ChessLocation(3, 3));
            ChessMove possiblemove5 = new ChessMove(testBishopLocation, new ChessLocation(5, 5));
            ChessMove possiblemove6 = new ChessMove(testBishopLocation, new ChessLocation(6, 6));
            ChessMove possiblemove7 = new ChessMove(testBishopLocation, new ChessLocation(7, 7));
            ChessMove possiblemove8 = new ChessMove(testBishopLocation, new ChessLocation(1, 7));
            ChessMove possiblemove9 = new ChessMove(testBishopLocation, new ChessLocation(2, 6));
            ChessMove possiblemove10 = new ChessMove(testBishopLocation, new ChessLocation(3, 5));
            ChessMove possiblemove11 = new ChessMove(testBishopLocation, new ChessLocation(5, 3));
            ChessMove possiblemove12 = new ChessMove(testBishopLocation, new ChessLocation(6, 2));
            ChessMove possiblemove13 = new ChessMove(testBishopLocation, new ChessLocation(7, 1));

            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackBishop, testBishopLocation, false);
            Assert.AreEqual(13, validMoves.Count, "Should have 13 possible moves for this bishop test");
            Assert.IsTrue(validMoves.Contains(possiblemove1));
            Assert.IsTrue(validMoves.Contains(possiblemove2));
            Assert.IsTrue(validMoves.Contains(possiblemove3));
            Assert.IsTrue(validMoves.Contains(possiblemove4));
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove6));
            Assert.IsTrue(validMoves.Contains(possiblemove7));
            Assert.IsTrue(validMoves.Contains(possiblemove8));
            Assert.IsTrue(validMoves.Contains(possiblemove9));
            Assert.IsTrue(validMoves.Contains(possiblemove10));
            Assert.IsTrue(validMoves.Contains(possiblemove11));
            Assert.IsTrue(validMoves.Contains(possiblemove12));
            Assert.IsTrue(validMoves.Contains(possiblemove13));

            // Test blocked diagonal movement
            clearTestBoard();
            testboard[testBishopLocation] = ChessPiece.BlackBishop;
            testboard[2, 2] = ChessPiece.BlackBishop;
            testboard[5, 3] = ChessPiece.BlackBishop;
            testboard[1, 7] = ChessPiece.BlackBishop;
            testboard[6, 6] = ChessPiece.BlackBishop;
            
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackBishop, testBishopLocation, false);
            Assert.AreEqual(4, validMoves.Count, "Should have 4 possible moves for this bishop test");
            Assert.IsTrue(validMoves.Contains(possiblemove4));
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove9));
            Assert.IsTrue(validMoves.Contains(possiblemove10));

            // Test capture diagonal movement
            clearTestBoard();
            testboard[testBishopLocation] = ChessPiece.BlackBishop;
            testboard[2, 2] = ChessPiece.WhiteBishop;
            testboard[5, 3] = ChessPiece.WhiteBishop;
            testboard[1, 7] = ChessPiece.WhiteBishop;
            testboard[6, 6] = ChessPiece.WhiteBishop;

            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackBishop, testBishopLocation, false);
            Assert.AreEqual(8, validMoves.Count, "Should have 8 possible moves for this bishop test");
            Assert.IsTrue(validMoves.Contains(possiblemove3));
            Assert.IsTrue(validMoves.Contains(possiblemove4));
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove6));
            Assert.IsTrue(validMoves.Contains(possiblemove8));
            Assert.IsTrue(validMoves.Contains(possiblemove9));
            Assert.IsTrue(validMoves.Contains(possiblemove10));
            Assert.IsTrue(validMoves.Contains(possiblemove11));
        }

        [TestMethod]
        public void testBishopReturnsProtectionMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testBishopLocation = new ChessLocation(4, 4);

            // Test diagonal movement
            clearTestBoard();
            testboard[testBishopLocation] = ChessPiece.BlackBishop;
            testboard[3, 3] = ChessPiece.BlackBishop;
            testboard[5, 3] = ChessPiece.BlackBishop;
            testboard[3, 5] = ChessPiece.BlackBishop;
            testboard[5, 5] = ChessPiece.BlackBishop;

            ChessMove protectionMove1 = new ChessMove(testBishopLocation, new ChessLocation(3, 3));
            ChessMove protectionMove2 = new ChessMove(testBishopLocation, new ChessLocation(5, 3));
            ChessMove protectionMove3 = new ChessMove(testBishopLocation, new ChessLocation(3, 5));
            ChessMove protectionMove4 = new ChessMove(testBishopLocation, new ChessLocation(5, 5));

            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackBishop, testBishopLocation, true);
            Assert.AreEqual(4, validMoves.Count, "Should have 4 possible moves for this bishop test");
            Assert.IsTrue(validMoves.Contains(protectionMove1));
            Assert.IsTrue(validMoves.Contains(protectionMove2));
            Assert.IsTrue(validMoves.Contains(protectionMove3));
            Assert.IsTrue(validMoves.Contains(protectionMove4));

        }

        [TestMethod]
        public void testGetQueenMovesReturnsValidMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testQueenLocation = new ChessLocation(4, 4);

            // Test all linear moves
            clearTestBoard();
            testboard[testQueenLocation] = ChessPiece.BlackQueen;
            ChessMove possiblemove1 = new ChessMove(testQueenLocation, new ChessLocation(4, 0));
            ChessMove possiblemove2 = new ChessMove(testQueenLocation, new ChessLocation(4, 1));
            ChessMove possiblemove3 = new ChessMove(testQueenLocation, new ChessLocation(4, 2));
            ChessMove possiblemove4 = new ChessMove(testQueenLocation, new ChessLocation(4, 3));
            ChessMove possiblemove5 = new ChessMove(testQueenLocation, new ChessLocation(4, 5));
            ChessMove possiblemove6 = new ChessMove(testQueenLocation, new ChessLocation(4, 6));
            ChessMove possiblemove7 = new ChessMove(testQueenLocation, new ChessLocation(4, 7));
            ChessMove possiblemove8 = new ChessMove(testQueenLocation, new ChessLocation(0, 4));
            ChessMove possiblemove9 = new ChessMove(testQueenLocation, new ChessLocation(1, 4));
            ChessMove possiblemove10 = new ChessMove(testQueenLocation, new ChessLocation(2, 4));
            ChessMove possiblemove11 = new ChessMove(testQueenLocation, new ChessLocation(3, 4));
            ChessMove possiblemove12 = new ChessMove(testQueenLocation, new ChessLocation(5, 4));
            ChessMove possiblemove13 = new ChessMove(testQueenLocation, new ChessLocation(6, 4));
            ChessMove possiblemove14 = new ChessMove(testQueenLocation, new ChessLocation(7, 4));
            ChessMove possiblemove15 = new ChessMove(testQueenLocation, new ChessLocation(0, 0));
            ChessMove possiblemove16 = new ChessMove(testQueenLocation, new ChessLocation(1, 1));
            ChessMove possiblemove17 = new ChessMove(testQueenLocation, new ChessLocation(2, 2));
            ChessMove possiblemove18 = new ChessMove(testQueenLocation, new ChessLocation(3, 3));
            ChessMove possiblemove19 = new ChessMove(testQueenLocation, new ChessLocation(5, 5));
            ChessMove possiblemove20 = new ChessMove(testQueenLocation, new ChessLocation(6, 6));
            ChessMove possiblemove21 = new ChessMove(testQueenLocation, new ChessLocation(7, 7));
            ChessMove possiblemove22 = new ChessMove(testQueenLocation, new ChessLocation(1, 7));
            ChessMove possiblemove23 = new ChessMove(testQueenLocation, new ChessLocation(2, 6));
            ChessMove possiblemove24 = new ChessMove(testQueenLocation, new ChessLocation(3, 5));
            ChessMove possiblemove25 = new ChessMove(testQueenLocation, new ChessLocation(5, 3));
            ChessMove possiblemove26 = new ChessMove(testQueenLocation, new ChessLocation(6, 2));
            ChessMove possiblemove27 = new ChessMove(testQueenLocation, new ChessLocation(7, 1));
            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackQueen, testQueenLocation, false);

            Assert.AreEqual(27, validMoves.Count, "Should have 27 possible moves for this queen test");
            Assert.IsTrue(validMoves.Contains(possiblemove1));
            Assert.IsTrue(validMoves.Contains(possiblemove2));
            Assert.IsTrue(validMoves.Contains(possiblemove3));
            Assert.IsTrue(validMoves.Contains(possiblemove4));
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove6));
            Assert.IsTrue(validMoves.Contains(possiblemove7));
            Assert.IsTrue(validMoves.Contains(possiblemove8));
            Assert.IsTrue(validMoves.Contains(possiblemove9));
            Assert.IsTrue(validMoves.Contains(possiblemove10));
            Assert.IsTrue(validMoves.Contains(possiblemove11));
            Assert.IsTrue(validMoves.Contains(possiblemove12));
            Assert.IsTrue(validMoves.Contains(possiblemove13));
            Assert.IsTrue(validMoves.Contains(possiblemove14));
            Assert.IsTrue(validMoves.Contains(possiblemove15));
            Assert.IsTrue(validMoves.Contains(possiblemove16));
            Assert.IsTrue(validMoves.Contains(possiblemove17));
            Assert.IsTrue(validMoves.Contains(possiblemove18));
            Assert.IsTrue(validMoves.Contains(possiblemove19));
            Assert.IsTrue(validMoves.Contains(possiblemove20));
            Assert.IsTrue(validMoves.Contains(possiblemove21));
            Assert.IsTrue(validMoves.Contains(possiblemove22));
            Assert.IsTrue(validMoves.Contains(possiblemove23));
            Assert.IsTrue(validMoves.Contains(possiblemove24));
            Assert.IsTrue(validMoves.Contains(possiblemove25));
            Assert.IsTrue(validMoves.Contains(possiblemove26));
            Assert.IsTrue(validMoves.Contains(possiblemove27));

            // Test blocked linear moves
            clearTestBoard();
            testboard[testQueenLocation] = ChessPiece.BlackQueen;
            testboard[2, 4] = ChessPiece.BlackQueen;
            testboard[4, 3] = ChessPiece.BlackQueen;
            testboard[7, 4] = ChessPiece.BlackQueen;
            testboard[4, 7] = ChessPiece.BlackQueen;
            testboard[2, 2] = ChessPiece.BlackQueen;
            testboard[5, 3] = ChessPiece.BlackQueen;
            testboard[1, 7] = ChessPiece.BlackQueen;
            testboard[6, 6] = ChessPiece.BlackQueen;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackQueen, testQueenLocation, false);

            Assert.AreEqual(9, validMoves.Count, "Should have 9 possible moves for this queen test");
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove6));
            Assert.IsTrue(validMoves.Contains(possiblemove11));
            Assert.IsTrue(validMoves.Contains(possiblemove12));
            Assert.IsTrue(validMoves.Contains(possiblemove13));
            Assert.IsTrue(validMoves.Contains(possiblemove18));
            Assert.IsTrue(validMoves.Contains(possiblemove19));
            Assert.IsTrue(validMoves.Contains(possiblemove23));
            Assert.IsTrue(validMoves.Contains(possiblemove24));

            // Test capture linear moves
            clearTestBoard();
            testboard[testQueenLocation] = ChessPiece.BlackQueen;
            testboard[2, 4] = ChessPiece.WhiteQueen;
            testboard[4, 3] = ChessPiece.WhiteQueen;
            testboard[7, 4] = ChessPiece.WhiteQueen;
            testboard[4, 7] = ChessPiece.WhiteQueen;
            testboard[2, 2] = ChessPiece.WhiteQueen;
            testboard[5, 3] = ChessPiece.WhiteQueen;
            testboard[1, 7] = ChessPiece.WhiteQueen;
            testboard[6, 6] = ChessPiece.WhiteQueen;
            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackQueen, testQueenLocation, false);

            Assert.AreEqual(17, validMoves.Count, "Should have 17 possible moves for this queen test");
            Assert.IsTrue(validMoves.Contains(possiblemove4));
            Assert.IsTrue(validMoves.Contains(possiblemove5));
            Assert.IsTrue(validMoves.Contains(possiblemove6));
            Assert.IsTrue(validMoves.Contains(possiblemove7));
            Assert.IsTrue(validMoves.Contains(possiblemove10));
            Assert.IsTrue(validMoves.Contains(possiblemove11));
            Assert.IsTrue(validMoves.Contains(possiblemove12));
            Assert.IsTrue(validMoves.Contains(possiblemove13));
            Assert.IsTrue(validMoves.Contains(possiblemove14));
            Assert.IsTrue(validMoves.Contains(possiblemove17));
            Assert.IsTrue(validMoves.Contains(possiblemove18));
            Assert.IsTrue(validMoves.Contains(possiblemove19));
            Assert.IsTrue(validMoves.Contains(possiblemove20));
            Assert.IsTrue(validMoves.Contains(possiblemove22));
            Assert.IsTrue(validMoves.Contains(possiblemove23));
            Assert.IsTrue(validMoves.Contains(possiblemove24));
            Assert.IsTrue(validMoves.Contains(possiblemove25));
        }

        [TestMethod]
        public void testQueenReturnsProtectionMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testQueenLocation = new ChessLocation(4, 4);
            testboard[testQueenLocation] = ChessPiece.BlackQueen;
            testboard[3, 3] = ChessPiece.BlackQueen;
            testboard[5, 3] = ChessPiece.BlackQueen;
            testboard[3, 5] = ChessPiece.BlackQueen;
            testboard[5, 5] = ChessPiece.BlackQueen;
            testboard[4, 3] = ChessPiece.BlackQueen;
            testboard[3, 4] = ChessPiece.BlackQueen;
            testboard[5, 4] = ChessPiece.BlackQueen;
            testboard[4, 5] = ChessPiece.BlackQueen;

            ChessMove protectionMove1 = new ChessMove(testQueenLocation, new ChessLocation(3, 3));
            ChessMove protectionMove2 = new ChessMove(testQueenLocation, new ChessLocation(5, 3));
            ChessMove protectionMove3 = new ChessMove(testQueenLocation, new ChessLocation(3, 5));
            ChessMove protectionMove4 = new ChessMove(testQueenLocation, new ChessLocation(5, 5));
            ChessMove protectionMove5 = new ChessMove(testQueenLocation, new ChessLocation(4, 3));
            ChessMove protectionMove6 = new ChessMove(testQueenLocation, new ChessLocation(3, 4));
            ChessMove protectionMove7 = new ChessMove(testQueenLocation, new ChessLocation(5, 4));
            ChessMove protectionMove8 = new ChessMove(testQueenLocation, new ChessLocation(4, 5));

            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackQueen, testQueenLocation, true);

            Assert.AreEqual(8, validMoves.Count, "Should have 8 possible moves for this queen test");
            Assert.IsTrue(validMoves.Contains(protectionMove1));
            Assert.IsTrue(validMoves.Contains(protectionMove2));
            Assert.IsTrue(validMoves.Contains(protectionMove3));
            Assert.IsTrue(validMoves.Contains(protectionMove4));
            Assert.IsTrue(validMoves.Contains(protectionMove5));
            Assert.IsTrue(validMoves.Contains(protectionMove6));
            Assert.IsTrue(validMoves.Contains(protectionMove7));
            Assert.IsTrue(validMoves.Contains(protectionMove8));
        }

        [TestMethod]
        public void testGetKingMovesReturnsValidMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testKingLocation = new ChessLocation(4, 4);

            // Test all moves
            clearTestBoard();
            testboard[testKingLocation] = ChessPiece.BlackKing;
            ChessMove possibleMove1 = new ChessMove(testKingLocation, new ChessLocation(3, 3));
            ChessMove possibleMove2 = new ChessMove(testKingLocation, new ChessLocation(4, 3));
            ChessMove possibleMove3 = new ChessMove(testKingLocation, new ChessLocation(5, 3));
            ChessMove possibleMove4 = new ChessMove(testKingLocation, new ChessLocation(3, 4));
            ChessMove possibleMove5 = new ChessMove(testKingLocation, new ChessLocation(5, 4));
            ChessMove possibleMove6 = new ChessMove(testKingLocation, new ChessLocation(3, 5));
            ChessMove possibleMove7 = new ChessMove(testKingLocation, new ChessLocation(4, 5));
            ChessMove possibleMove8 = new ChessMove(testKingLocation, new ChessLocation(5, 5));

            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackKing, testKingLocation, false);
            Assert.AreEqual(8, validMoves.Count, "Should have 8 possible moves for this king test");
            Assert.IsTrue(validMoves.Contains(possibleMove1));
            Assert.IsTrue(validMoves.Contains(possibleMove2));
            Assert.IsTrue(validMoves.Contains(possibleMove3));
            Assert.IsTrue(validMoves.Contains(possibleMove4));
            Assert.IsTrue(validMoves.Contains(possibleMove5));
            Assert.IsTrue(validMoves.Contains(possibleMove6));
            Assert.IsTrue(validMoves.Contains(possibleMove7));
            Assert.IsTrue(validMoves.Contains(possibleMove8));

            // Test blocked moves
            clearTestBoard();
            testboard[testKingLocation] = ChessPiece.BlackKing;
            testboard[3, 3] = ChessPiece.BlackKing;
            testboard[4, 3] = ChessPiece.BlackKing;
            testboard[5, 3] = ChessPiece.BlackKing;
            testboard[3, 4] = ChessPiece.BlackKing;
            testboard[5, 4] = ChessPiece.BlackKing;
            testboard[3, 5] = ChessPiece.BlackKing;
            testboard[4, 5] = ChessPiece.BlackKing;
            testboard[5, 5] = ChessPiece.BlackKing;

            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackKing, testKingLocation, false);
            Assert.AreEqual(0, validMoves.Count, "Should have 0 possible moves for this king test");

            // Test capture moves
            clearTestBoard();
            testboard[testKingLocation] = ChessPiece.BlackKing;
            testboard[3, 3] = ChessPiece.WhiteKing;
            testboard[4, 3] = ChessPiece.WhiteKing;
            testboard[5, 3] = ChessPiece.WhiteKing;
            testboard[3, 4] = ChessPiece.WhiteKing;
            testboard[5, 4] = ChessPiece.WhiteKing;
            testboard[3, 5] = ChessPiece.WhiteKing;
            testboard[4, 5] = ChessPiece.WhiteKing;
            testboard[5, 5] = ChessPiece.WhiteKing;

            validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackKing, testKingLocation, false);
            Assert.AreEqual(8, validMoves.Count, "Should have 8 possible moves for this king test");
            Assert.IsTrue(validMoves.Contains(possibleMove1));
            Assert.IsTrue(validMoves.Contains(possibleMove2));
            Assert.IsTrue(validMoves.Contains(possibleMove3));
            Assert.IsTrue(validMoves.Contains(possibleMove4));
            Assert.IsTrue(validMoves.Contains(possibleMove5));
            Assert.IsTrue(validMoves.Contains(possibleMove6));
            Assert.IsTrue(validMoves.Contains(possibleMove7));
            Assert.IsTrue(validMoves.Contains(possibleMove8));
        }

        [TestMethod]
        public void testKingReturnsProtectionMoves()
        {
            testboard = new ChessBoard();
            ChessLocation testKingLocation = new ChessLocation(4, 4);
            testboard[testKingLocation] = ChessPiece.BlackKing;
            testboard[3, 3] = ChessPiece.BlackKing;
            testboard[5, 3] = ChessPiece.BlackKing;
            testboard[3, 5] = ChessPiece.BlackKing;
            testboard[5, 5] = ChessPiece.BlackKing;
            testboard[4, 3] = ChessPiece.BlackKing;
            testboard[3, 4] = ChessPiece.BlackKing;
            testboard[5, 4] = ChessPiece.BlackKing;
            testboard[4, 5] = ChessPiece.BlackKing;

            ChessMove protectionMove1 = new ChessMove(testKingLocation, new ChessLocation(3, 3));
            ChessMove protectionMove2 = new ChessMove(testKingLocation, new ChessLocation(5, 3));
            ChessMove protectionMove3 = new ChessMove(testKingLocation, new ChessLocation(3, 5));
            ChessMove protectionMove4 = new ChessMove(testKingLocation, new ChessLocation(5, 5));
            ChessMove protectionMove5 = new ChessMove(testKingLocation, new ChessLocation(4, 3));
            ChessMove protectionMove6 = new ChessMove(testKingLocation, new ChessLocation(3, 4));
            ChessMove protectionMove7 = new ChessMove(testKingLocation, new ChessLocation(5, 4));
            ChessMove protectionMove8 = new ChessMove(testKingLocation, new ChessLocation(4, 5));

            List<ChessMove> validMoves = MoveGenerator.validMovesForPiece(testboard, ChessPiece.BlackKing, testKingLocation, true);

            Assert.AreEqual(8, validMoves.Count, "Should have 8 possible moves for this king test");
            Assert.IsTrue(validMoves.Contains(protectionMove1));
            Assert.IsTrue(validMoves.Contains(protectionMove2));
            Assert.IsTrue(validMoves.Contains(protectionMove3));
            Assert.IsTrue(validMoves.Contains(protectionMove4));
            Assert.IsTrue(validMoves.Contains(protectionMove5));
            Assert.IsTrue(validMoves.Contains(protectionMove6));
            Assert.IsTrue(validMoves.Contains(protectionMove7));
            Assert.IsTrue(validMoves.Contains(protectionMove8));
        }
        
        [TestMethod]
        public void testCannotMakeMoveThatPlacesKingInCheck()
        {
            testboard = new ChessBoard();
            clearTestBoard();

            testboard[4, 4] = ChessPiece.WhiteKing;
            testboard[3, 3] = ChessPiece.WhiteBishop;
            testboard[1, 1] = ChessPiece.BlackBishop;
            testboard[5, 1] = ChessPiece.BlackPawn;
            testboard[1, 5] = ChessPiece.BlackPawn;

            // Testing for moves of the white bishop so he doens't move and place the king in check
            ChessMove expectedMove1 = new ChessMove(new ChessLocation(3, 3), new ChessLocation(2, 2));
            ChessMove expectedMove2 = new ChessMove(new ChessLocation(3, 3), new ChessLocation(1, 1));
            ChessMove illegalMove1 = new ChessMove(new ChessLocation(3,3), new ChessLocation(4,2));
            ChessMove illegalMove2 = new ChessMove(new ChessLocation(3,3), new ChessLocation(2,4));

            List<ChessMove> availableMoves = MoveGenerator.getAllMoves(testboard, ChessColor.White, false);
            List<ChessMove> legalMoves = MoveGenerator.getAllLegalMoves(testboard, availableMoves, ChessColor.White);

            Assert.IsFalse(legalMoves.Contains(illegalMove1), "Bishop should not be able to move from 3,3 to 4,2");
            Assert.IsFalse(legalMoves.Contains(illegalMove2), "Bishop should not be able to move from 3,3 to 2,4");
        }

        [TestMethod]
        public void testCannotMoveKingIntoCheck()
        {
            StudentAI.StudentAI ai = new StudentAI.StudentAI();

            testboard = new ChessBoard();
            clearTestBoard();

            testboard[4, 4] = ChessPiece.WhiteKing;
            testboard[3, 3] = ChessPiece.WhiteBishop;
            testboard[1, 1] = ChessPiece.BlackBishop;
            testboard[6, 3] = ChessPiece.BlackPawn;
            testboard[1, 5] = ChessPiece.BlackPawn;

            ChessMove illegalMove1 = new ChessMove(new ChessLocation(4, 4), new ChessLocation(5, 4));

            List<ChessMove> availableMoves = MoveGenerator.getAllMoves(testboard, ChessColor.White, false);
            List<ChessMove> legalMoves = MoveGenerator.getAllLegalMoves(testboard, availableMoves, ChessColor.White);

            Assert.IsFalse(legalMoves.Contains(illegalMove1), "King should not be able to move from 4,4 to 5,4");
        }

        [TestMethod]
        public void testKingInCheckCannotMoveToAnotherPlaceInCheck()
        {
            StudentAI.StudentAI ai = new StudentAI.StudentAI();

            testboard = new ChessBoard();
            clearTestBoard();

            testboard[4, 4] = ChessPiece.WhiteKing;
            testboard[1, 1] = ChessPiece.BlackBishop;
            testboard[6, 3] = ChessPiece.BlackPawn;
            testboard[1, 5] = ChessPiece.BlackPawn;

            ChessMove illegalMove1 = new ChessMove(new ChessLocation(4, 4), new ChessLocation(3, 3));
            ChessMove illegalMove2 = new ChessMove(new ChessLocation(4, 4), new ChessLocation(5, 4));

            List<ChessMove> availableMoves = MoveGenerator.getAllMoves(testboard, ChessColor.White, false);
            List<ChessMove> legalMoves = MoveGenerator.getAllLegalMoves(testboard, availableMoves, ChessColor.White);

            Assert.IsFalse(legalMoves.Contains(illegalMove1), "King should not be able to move from 4,4 to 3,3");
            Assert.IsFalse(legalMoves.Contains(illegalMove2), "King should not be able to move from 4,4 to 5,4");
        }
    }
}
